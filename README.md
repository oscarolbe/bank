# Bank

Test Bank for Pagantis

## What includes?

- Phoenix project boilerplate https://github.com/alvarolizama/phoenix-boilerplate

## Requirements

- Docker
- Docker Compose
- Cmake

Copy the .env.template file to .env
```
cp .env.template .env
```

## Run the project for development

### Dependencies

- Install Docker for Mac Os

```shell
Descargar https://download.docker.com/mac/stable/Docker.dmg
```

- Install Docker in Linux

```shell
curl -sSL https://get.docker.com/ | sh
sudo usermod -aG docker $(whoami)
curl -L "https://github.com/docker/compose/releases/download/1.21.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

```

- Install cmake in macOS

```shell
brew install cmake
```

- Install cmake in Linux

```shell
apt-get install cmake
```

### Environment

- Just run the next command for build the environment

```shell
make bootstrap
```

- Test the installation running the service and open **http://localhost**

```shell
make start
```

Enter to: http://localhost:8080/

If you execute two times for any reason please make an `make ecto.reset`

## Commands

### Bootrstrap commands

- make bootstrap: Create an enviroment and setup the project
- make reset: Reset all setups

### Docker commands

- make start: Start applications services
- make restart.phx: Restart web service
- make restart.postgres: Restart postgres service
- make stop.phx: Stop web service
- make stop.postgres: Stop postgres service
- make logs.phx: Show logs for web service
- make logs.postgres: Show logs for postgres service
- make shell.phx: Open a shell in web service container
- make shell.postgres: Open a shell in postgres service container

### Development commands

- make test: Run tests
- make test.shell: Open a shell for testing
- make credo: Run credo
- make coverage: Run coverage reports
- make gettext: Compile gettext
- make routes: Show routes
- make deps.update: Clean and update dependencies
- make ecto.reset: Delete database and recreate all
- make ecto.setup: Setup database and migrations
- make ecto.migrate: Run migrations
- make npm.install: Run npm install

## Make a release

Create a new release

    make build.release

Build a new docker images

    docker build -t <NAME:TAG> .

Submit to a repository

    docker push <NAME:TAG>

## Part 3

# How would you improve your solution? What would be the next steps?
  * Validate accounts structs are valid in the TransferAgent
  * Create Customer.ex schema instead of using the client name for the bank account
  * Do currency persist in postgres with Money library postgres types
  * Add input text to send different amounts
  * Specify type of movement or transaction (debit, credit, commission)

# After implementation, is there any design decision that you would have done different?
  * Create movements table that replaces transactions. All movements will have a transaction parent that will produce that movement (Charge, commission, withdrawal, reclaim)

# How would you adapt your solution if transfers are not instantaneous?
  * Each function in the `with` statement of `TransferAgent` module has to be a service that supports retries, queues and/or PubSub system
  * The services proposed are: `TransferAgentService` that will store the action and begin with withdrawal process and call BankServiceProvider for `bank A` and store the action. Each of this actions will generate the rollback action when some of this steps fail.
  * Call the different services or apps with VM nodes to be faster than an api
