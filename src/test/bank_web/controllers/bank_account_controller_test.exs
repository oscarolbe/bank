defmodule BankWeb.BankAccountControllerTest do
  use BankWeb.ConnCase

  alias Bank.Accounts.BankAccountManager
  import Bank.Factory

  @create_attrs %{client: "Oscar", amount: 42, bank: :bank_a}
  @update_attrs %{amount: 43}
  @invalid_attrs %{amount: nil}

  def fixture(:bank_account) do
    insert(:bank_account)
  end

  describe "index" do
    test "lists all bank_accounts", %{conn: conn} do
      conn = get(conn, Routes.bank_account_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Bank accounts"
    end
  end

  describe "new bank_account" do
    test "renders form", %{conn: conn} do
      conn = get(conn, Routes.bank_account_path(conn, :new))
      assert html_response(conn, 200) =~ "New Bank account"
    end
  end

  describe "create bank_account" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, Routes.bank_account_path(conn, :create), bank_account: @create_attrs)

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.bank_account_path(conn, :show, id)

      conn = get(conn, Routes.bank_account_path(conn, :show, id))
      assert html_response(conn, 200) =~ "Show Bank account"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.bank_account_path(conn, :create), bank_account: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Bank account"
    end
  end

  describe "edit bank_account" do
    setup [:create_bank_account]

    test "renders form for editing chosen bank_account", %{conn: conn, bank_account: bank_account} do
      conn = get(conn, Routes.bank_account_path(conn, :edit, bank_account))
      assert html_response(conn, 200) =~ "Edit Bank account"
    end
  end

  defp create_bank_account(_) do
    bank_account = fixture(:bank_account)
    {:ok, bank_account: bank_account}
  end
end
