defmodule Bank.Factory do
  use ExMachina.Ecto, repo: Bank.Repo

  alias Bank.Accounts.BankAccount

  def bank_account_factory do
    %BankAccount{
      client: "Jose",
      bank: :bank_a,
      amount: Money.parse!("30,000.00")
    }
  end
end
