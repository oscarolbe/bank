defmodule Bank.Accounts.BankAccountManagerTest do
  use Bank.DataCase

  import Bank.Factory
  alias Bank.Accounts.BankAccount
  alias Bank.Accounts.BankAccountManager

  @valid_attrs %{
    client: "Oscar",
    bank: :bank_a,
    amount: Money.new(0)
  }

  describe "bank_accounts" do

    test "list_bank_accounts/0 returns all bank_accounts" do
      bank_account = insert(:bank_account)
      assert BankAccountManager.list_bank_accounts() == [bank_account]
    end

    test "get_bank_account!/1 returns the bank_account with given id" do
      bank_account = insert(:bank_account)
      assert BankAccountManager.get_bank_account!(bank_account.id) == bank_account
    end

    test "create_bank_account/1 with valid data creates a bank_account" do
      assert {:ok, %BankAccount{} = bank_account} = BankAccountManager.create_bank_account(@valid_attrs)
      assert bank_account.amount == Money.new(0)
    end

    test "change_bank_account/1 returns a bank_account changeset" do
      bank_account = insert(:bank_account)
      assert %Ecto.Changeset{} = BankAccountManager.change_bank_account(bank_account)
    end
  end
end
