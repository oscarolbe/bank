defmodule Bank.Accounts.TransferManagerTest do
  use Bank.DataCase

  import Bank.Factory
  alias Bank.Accounts.TransferManager

  test "Create all involved transfers" do
    bank_account_jose = insert(:bank_account)
    bank_account_maria = insert(:bank_account, bank: :bank_b)
    total_debt = Money.parse!("1,000.00")
    commission = Money.parse!("5.00")

    [{:ok, transfer_out}, {:ok, transfer_commission}, {:ok, transfer_in}] =
      TransferManager.create(bank_account_jose, bank_account_maria, total_debt, commission)

    assert transfer_out.amount == Money.neg(total_debt)
    assert transfer_commission.amount == commission
    assert transfer_in.amount == total_debt
  end
end
