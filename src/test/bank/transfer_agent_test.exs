defmodule Bank.TransferAgentTest do
  use Bank.DataCase

  import Bank.Factory
  import Mock
  alias Bank.TransferAgent
  alias Bank.Accounts.Transfer
  alias Bank.Accounts.TransferManager
  alias Bank.Accounts.BankA
  alias Bank.Accounts.BankB

  test "Transfer failed when transfer limit is reached (1,000 EUR)" do
    bank_account_jose = insert(:bank_account)
    bank_account_maria = insert(:bank_account, bank: :bank_b)

    amount = Money.parse!("20,000.00")

    result =
      TransferAgent.transfer(bank_account_jose, bank_account_maria, amount)

    assert result == {:error, :over_limit}
  end

  test "Transfer failed when first source bank has internal error" do
    with_mocks([
      { BankA, [:passthrough], [has_errors: fn -> true end] },
    ]) do
      bank_account_jose = insert(:bank_account)
      bank_account_maria = insert(:bank_account, bank: :bank_b)

      amount = Money.parse!("1,000.00")

      result =
        TransferAgent.transfer(bank_account_jose, bank_account_maria, amount)

      assert result == {:error, :transfer_error}
    end
  end

  test "Transfer failed when has first account has not enought money" do
    with_mocks([
      { BankA, [:passthrough], [has_errors: fn -> false end] },
      { BankB, [:passthrough], [has_errors: fn -> false end] },
    ]) do
      bank_account_jose = insert(:bank_account, amount: 100)
      bank_account_maria = insert(:bank_account, bank: :bank_b)

      amount = Money.parse!("1,000.00")

      result =
        TransferAgent.transfer(bank_account_jose, bank_account_maria, amount)

      assert result == {:error, :insufficient_balance}
    end
  end

  test "Transfer success with Jose, Maria and Antonio" do
    with_mocks([
      { BankA, [:passthrough], [has_errors: fn -> false end] },
      { BankB, [:passthrough], [has_errors: fn -> false end] },
    ]) do
      account_jose = insert(:bank_account)
      account_maria = insert(:bank_account, bank: :bank_b)
      account_antonio = insert(:bank_account, bank: :bank_b)

      amount = Money.parse!("1,000.00")

      {:ok, account_jose, account_maria} =
        TransferAgent.transfer(account_jose, account_maria, amount)

      assert account_jose.amount == Money.parse!("28,995.00")
      assert account_maria.amount == Money.parse!("31,000.00")

      {:ok, account_antonio, account_maria} =
        TransferAgent.transfer(account_antonio, account_maria, amount)

      assert account_antonio.amount == Money.parse!("29000.00")
      assert account_maria.amount == Money.parse!("32,000.00")
    end
  end
end
