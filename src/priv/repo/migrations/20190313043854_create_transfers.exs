defmodule Bank.Repo.Migrations.CreateTransfers do
  use Ecto.Migration

  def change do
    create table(:transfers) do
      add :bank_account_from_id, references(:bank_accounts)
      add :bank_account_to_id, references(:bank_accounts)
      add :amount, :integer

      timestamps()
    end
  end
end
