defmodule Bank.Repo.Migrations.CreateBankAccounts do
  use Ecto.Migration

  def change do
    create table(:bank_accounts) do
      add :client, :string
      add :amount, :integer, [null: false]
      add :bank, :integer

      timestamps()
    end
  end
end
