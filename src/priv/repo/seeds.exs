# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Bank.Repo.insert!(%WebApp.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.


alias Bank.Accounts.BankAccountManager
alias Bank.Accounts.BankAccount
alias Bank.Accounts.TransferManager

BankAccountManager.create_bank_account(%{
  client: "Jose",
  bank: :bank_a,
  amount: Money.parse!("10,000.00")
})

BankAccountManager.create_bank_account(%{
  client: "Maria",
  bank: :bank_b,
  amount: Money.parse!("10,000.00")
})

BankAccountManager.create_bank_account(%{
  client: "Antonio",
  bank: :bank_b,
  amount: Money.parse!("10,000.00")
})
