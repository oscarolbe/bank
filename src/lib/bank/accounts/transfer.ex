defmodule Bank.Accounts.Transfer do
  use Ecto.Schema

  alias Bank.Accounts.BankAccount
  alias Money.Ecto.Amount.Type, as: MoneyType
  import Ecto.Changeset

  schema "transfers" do
    belongs_to :bank_account_from, BankAccount
    belongs_to :bank_account_to, BankAccount
    field :amount, MoneyType

    timestamps()
  end

  @doc false
  def changeset(bank_account, account_from, account_to, attrs) do
    bank_account
    |> cast(attrs, [:bank_account_from_id, :bank_account_to_id, :amount])
    |> put_assoc(:bank_account_from, account_from)
    |> put_assoc(:bank_account_to, account_to)
    |> validate_required([:amount])
  end
end
