defmodule Bank.Accounts.BankAccount do
  use Ecto.Schema

  alias Bank.Accounts.Transfer
  alias Money.Ecto.Amount.Type, as: MoneyType
  import Ecto.Changeset

  schema "bank_accounts" do
    field :client, :string
    field :bank, BankEnum
    field :amount, MoneyType

    many_to_many :transfers_from, Transfer, join_through: "transfers",  join_keys: [
      bank_account_from_id: :id,
      bank_account_to_id: :id
    ]
    many_to_many :transfers_to, Transfer, join_through: "transfers", join_keys: [
      bank_account_to_id: :id,
      bank_account_from_id: :id
    ]

    timestamps()
  end

  @doc false
  def changeset(bank_account, attrs) do
    bank_account
    |> cast(attrs, [:client, :bank, :amount])
    |> validate_required([:client, :bank, :amount])
  end
end
