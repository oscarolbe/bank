defmodule Bank.Accounts.TransferManager do

  alias Bank.Repo
  alias Bank.Accounts.Transfer
  alias Bank.Accounts.BankAccount

  @doc """
  Creates the transactions involved when making a tranfer. A list with transfer
  out, transfer in and the transfer commission. When the commission not apply
  it also returns a tuple like this `{:ok, nil}`
  """
  @spec create(BankAccount.t(), BankAccount.t(), Money.t(), Money.t()) :: [Transfer.t()]
  def create(account_from, account_to, amount, commision) do
    transfer_out = _create(account_from, account_to, Money.neg(amount))
    transfer_out_comission = _create(account_from, account_to, commision)
    transfer_in = _create(account_from, account_to, amount)

    [transfer_out, transfer_out_comission, transfer_in]
  end

  @doc "Creates one transfer in db"
  @spec _create(BankAccount.t(), BankAccount.t(), Money.t())
  :: {:ok, nil | Transfer.t()} | {:error, any()}
  defp _create(account_from, account_to, %Money{amount: 0}) do
    {:ok, nil}
  end
  defp _create(account_from, account_to, %Money{} = amount) do
    %Transfer{}
    |> Transfer.changeset(account_from, account_to, %{amount: amount})
    |> Repo.insert()
  end
end
