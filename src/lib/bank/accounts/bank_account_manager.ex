defmodule Bank.Accounts.BankAccountManager do
  @moduledoc """
  The Accounts context.
  """

  import Ecto.Query, warn: false
  alias Bank.Repo

  alias Bank.Accounts.BankAccount

  @doc """
  Returns the list of bank_accounts.

  ## Examples

      iex> list_bank_accounts()
      [%BankAccount{}, ...]

  """
  def list_bank_accounts do
    Repo.all(BankAccount)
  end

  @doc """
  Gets a single bank_account.

  Raises `Ecto.NoResultsError` if the Bank account does not exist.

  ## Examples

      iex> get_bank_account!(123)
      %BankAccount{}

      iex> get_bank_account!(456)
      ** (Ecto.NoResultsError)

  """
  def get_bank_account!(id), do: Repo.get!(BankAccount, id)

  @doc """
  Creates a bank_account.

  ## Examples

      iex> create_bank_account(%{field: value})
      {:ok, %BankAccount{}}

      iex> create_bank_account(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_bank_account(attrs \\ %{}) do
    %BankAccount{}
    |> BankAccount.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates balance for the account when the accounts has enought money to pay
  the transfer and commission, or returns an error by insuficient balance
  """
  @spec withdrawal(BankAccount.t(), Money.t(), Money.t()) :: tuple()
  def withdrawal(account, amount, commission) do
    remaining_amount =
      account
      |> Map.get(:amount)
      |> Money.subtract(amount)
      |> Money.subtract(commission)

    if Money.compare(remaining_amount, Money.new(0)) >= 0 do
      update_amount(account, remaining_amount)
    else
      {:error, :insufficient_balance}
    end
  end

  @doc """
  Adds money transfered by the source account.
  """
  @spec deposit(BankAccount.t(), Money.t()) :: tuple()
  def deposit(account, amount) do
    remaining_amount =
      account
      |> Map.get(:amount)
      |> Money.add(amount)

    update_amount(account, remaining_amount)
  end

  @spec update_amount(BankAccount.t(), Money.t()) :: tuple()
  def update_amount(account, amount) do
    account
    |> BankAccount.changeset(%{amount: amount})
    |> Repo.update()
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking bank_account changes.

  ## Examples

      iex> change_bank_account(bank_account)
      %Ecto.Changeset{source: %BankAccount{}}

  """
  def change_bank_account(%BankAccount{} = bank_account) do
    BankAccount.changeset(bank_account, %{})
  end

  @doc "Search bank account by name"
  @spec search_by_name(String.t()) :: BankAccount.t()
  def search_by_name(name) do
    BankAccount
    |> where([b], b.client == ^name)
    |> Repo.one!()
  end
end
