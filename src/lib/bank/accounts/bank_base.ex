defmodule Bank.Accounts.BankBase do
  @moduledoc false

  alias Bank.Accounts.BankAccount
  alias __MODULE__

  defmacro __using__(opts) do
    bank = Keyword.get(opts, :bank)
    quote do
      @behaviour BankBase

      def transfer(%BankAccount{bank: unquote(bank)}, %BankAccount{bank: unquote(bank)}, amount) do
        # Save in its own database or notify with service
        {:ok, "Transfer success"}
      end
      def transfer(account_from, account_to, amount) do
        if __MODULE__.has_errors() do
          {:error, :transfer_error, __MODULE__}
        else
          {:ok, "Transfer success"}
        end
      end

      @doc "Returns a random boolean to mock status from the transfer"
      @spec has_errors() :: boolean
      def has_errors() do
        random_number = :rand.uniform(3)
        random_number != 1
      end

      def rollback_transaction(account_from, account_to, amount) do
        # Rollback transaction
      end

      def create_commission(account_from, account_to) do
        {:ok, "Commission success"}
      end
    end
  end

  @callback transfer(BankAccount.t(), BankAccount.t(), Money.t()) :: {:ok, any()} | {:error, any()}
end
