defmodule Bank.TransferAgent do
  @moduledoc """
  Core transfer module
  """

  alias Bank.Accounts.TransferManager
  alias Bank.Accounts.BankAccountManager
  alias Bank.Accounts.BankAccount
  alias Bank.Accounts.BankA
  alias Bank.Accounts.BankB

  @doc """
  Transfer money from two different accounts.
  When the amount is over 10,000 EUR it returns an error
  """
  @spec transfer(BankAccount.t(), BankAccount.t(), Money.t()) :: tuple()
  def transfer(account, account, %Money{amount: amount}) do
    {:error, :same_account}
  end
  def transfer(_from, _to, %Money{amount: amount}) when amount > 100_000 do
    {:error, :over_limit}
  end
  def transfer(account_from, account_to, %Money{} = amount) do
    # TODO: Validate accounts or refresh with the last changes
    bank_from = get_bank_module(account_from)
    bank_to = get_bank_module(account_to)

    commission = get_commission(bank_from, bank_to)

    with {:ok, account1} <- BankAccountManager.withdrawal(account_from, amount, commission),
         {:ok, account2} <- BankAccountManager.deposit(account_to, amount),
         [{:ok, _}, {:ok, _}, {:ok, _}] <-
           TransferManager.create(account_from, account_to, amount, commission),
         {:ok, _} <- bank_from.transfer(account_from, account_to, amount),
         {:ok, _} <- bank_to.transfer(account_from, account_to, amount),
         {:ok, _} <- bank_from.create_commission(account_from, account_to)
    do
      {:ok, account1, account2}
    else
      # TODO: Handle rollback operations or update transactions status
      {:error, :transfer_error, bank_to} ->
        bank_from.rollback_transaction(account_from, account_to, amount)
        {:error, :transfer_error}
      error -> error
    end
  end

  @doc "Gets specific module Bank by the name"
  @spec get_bank_module(BankAccount.t()) :: module()
  defp get_bank_module(%BankAccount{bank: :bank_a}), do: BankA
  defp get_bank_module(%BankAccount{bank: :bank_b}), do: BankB

  @doc "Gets the commission according with the bank source and receptor"
  @spec get_commission(struct(), struct()) :: Money.t()
  defp get_commission(same_bank, same_bank), do: Money.new(0)
  defp get_commission(_, _), do: Money.parse!("5.00")
end
