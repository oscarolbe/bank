defmodule :hooks do
  require Logger
  def migrate do
    path = Application.app_dir(:bank, "priv/repo/migrations")
    Ecto.Migrator.run(Bank.Repo, path, :up, all: true)
    :init.stop()
  end
end

