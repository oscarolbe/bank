defmodule BankWeb.BankAccountController do
  use BankWeb, :controller

  alias Bank.Accounts.BankAccountManager
  alias Bank.Accounts.BankAccount

  def index(conn, _params) do
    bank_accounts = BankAccountManager.list_bank_accounts()
    render(conn, "index.html", bank_accounts: bank_accounts)
  end

  def new(conn, _params) do
    changeset = BankAccountManager.change_bank_account(%BankAccount{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"bank_account" => bank_account_params}) do
    case BankAccountManager.create_bank_account(bank_account_params) do
      {:ok, bank_account} ->
        conn
        |> put_flash(:info, "Bank account created successfully.")
        |> redirect(to: Routes.bank_account_path(conn, :show, bank_account))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    bank_account = BankAccountManager.get_bank_account!(id)
    render(conn, "show.html", bank_account: bank_account)
  end

  def edit(conn, %{"id" => id}) do
    bank_account = BankAccountManager.get_bank_account!(id)
    changeset = BankAccountManager.change_bank_account(bank_account)
    render(conn, "edit.html", bank_account: bank_account, changeset: changeset)
  end
end
