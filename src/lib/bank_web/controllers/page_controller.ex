defmodule BankWeb.PageController do
  use BankWeb, :controller

  alias Bank.TransferAgent
  alias Bank.Accounts.BankAccountManager

  def index(conn, _params) do
    maria_account = BankAccountManager.search_by_name("Maria")
    jose_account = BankAccountManager.search_by_name("Jose")
    antonio_account = BankAccountManager.search_by_name("Antonio")

    render(
      conn,
      "index.html",
      maria_account: maria_account,
      jose_account: jose_account,
      antonio_account: antonio_account
    )
  end

  def transfer(conn, %{"from" => from, "to" => to}) do
    # TODO: Use account identifier to find account
    from_account = BankAccountManager.search_by_name(from)
    to_account = BankAccountManager.search_by_name(to)
    amount = Money.parse!("1,000.00")
    TransferAgent.transfer(from_account, to_account, amount)

    redirect(conn, to: "/")
  end
end
