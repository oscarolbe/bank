use Mix.Config

config :bank,
  ecto_repos: [Bank.Repo]

config :bank, BankWeb.Endpoint,
  http: [port: System.get_env("PHX_HTTP_PORT")],
  url: [host: System.get_env("PHX_URL_HOST")],
  secret_key_base: "d0sgB4Lsn/W8VLliN7mJjcXkTjUQbCUrYCJCUpAk2er7JBYrMaw3N7bWXFfOz8uB",
  render_errors: [view: BankWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Bank.PubSub,
           adapter: Phoenix.PubSub.PG2]

config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

config :bank, Bank.Repo,
  adapter: Ecto.Adapters.Postgres,
  pool_size: 10

config :phoenix, :json_library, Jason

config :money,
  default_currency: :EUR

import_config "#{Mix.env()}.exs"
