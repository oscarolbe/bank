use Mix.Config

config :bank, BankWeb.Endpoint,
  server: false

config :logger, level: :warn

config :bank, Bank.Repo,
  adapter: Ecto.Adapters.Postgres,
  pool: Ecto.Adapters.SQL.Sandbox
